import cats.effect._
import cats.syntax._
import cats.Functor
import cats.Applicative
import cats.implicits.catsStdInstancesForOption
import scala.language.higherKinds

// algebra for greeting
trait GreetingAlg[F[_]]{
  def greet:F[String]
}

// algebra for message
trait MessageAlg[F[_]]{
  def message:F[String]
}

// interpreter option dsl
object Hello extends GreetingAlg[Option]{
  def greet = Some("Hello")
}

// interpreter option dsl
object Message extends MessageAlg[Option]{
  def message = Some("With effect Option")
}

// interpreter io dsl
object HelloIO extends GreetingAlg[IO]{
  def greet = IO.pure("Hello IO")
}

// interpreter io dsl
object MessageIO extends MessageAlg[IO]{
  def message = IO.pure("With effect IO")
}

// program as value. we can store, combine, pass around
object GreetingProgram{
  def program[F[_]](g:GreetingAlg[F])(implicit F:Functor[F]):F[String] = 
    F.map(g.greet)(_ + " :)")
}

// program requiring a functor with the ability to suspend an impure call
object GreetingProgramRandom{
  def program[F[_]](g:GreetingAlg[F])(implicit F:Sync[F]):F[String]= 
    F.map2(g.greet, F.delay(scala.util.Random.nextInt(4)))((x,y)=> x + " " + ":)" * y)
}

// module can combine programs
object GreetingMessageModule{
  def program[F[_]](g:GreetingAlg[F], m:MessageAlg[F])(implicit F:Applicative[F]):F[String] = 
    F.map2(g.greet, m.message)((x, y) => x + " " + y)
}

// construct a program for Greeting algebra using the Option interpreter
val pr1:Option[String] = GreetingProgram.program(Hello)
//  fold effect to pure value
pr1.fold("Error")(identity)
  
// construct a program for Greeting algebra using the IO interpreter
val pr2 = GreetingProgram.program(HelloIO)
// execute with unsafe run
pr2.unsafeRunSync() 

//below will not compile - hello interpreter does not provide Sync so cannot suspend an impure call 
//val pr3 = GreetingProgramRandom.program(Hello) // could not find implicit value for parameter F: cats.effect.Sync[[+A]Option[A]]scalac 
  
val pr4 = GreetingProgramRandom.program(HelloIO)
pr4.unsafeRunSync()

val pr5 = GreetingMessageModule.program(Hello, Message)
pr5.fold("Error")(identity)

val pr6 = GreetingMessageModule.program(HelloIO, MessageIO)
pr6.unsafeRunSync()

// outside of a worksheet unsafeRunSync should only be called at the
// edge of the world. cats provides the IOApp trait to execute an IO type
object Main extends IOApp {
  def run(args: List[String]): IO[ExitCode] = 
    for {
      _ <- IO(println(pr1.fold("")(identity)))
    } yield(ExitCode.Success)
}
