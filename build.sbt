import Dependencies._

ThisBuild / scalaVersion     := "2.12.10"

lazy val root = (project in file("."))
  .settings(
    name := "tagless final example",
    libraryDependencies += scalaTest % Test
  )

libraryDependencies += "org.typelevel" %% "cats-effect" % "2.0.0"
