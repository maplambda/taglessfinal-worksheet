# Tagless-final/mtl style effect polymorphism
The tagless-final or mtl style approach provides a mechanism for interpreting a higher kinded type parameter `F[_]` based dsl using typeclasses.
## Greeting + Message dsl/algebra

This repository contains a scala worksheet example of a Greeting and Message dsl and programs using the [Cats](https://typelevel.org/cats/) library for typeclasses.

In the example below the greeting dsl is instantiated with the ```Option``` type. The worksheet contains an example of instantiating the same dsl using the ```IO``` type.

```scala
// define algebra of combinators
trait GreetingAlg[F[_]]{
  def greet:F[String]
}
//provide a concerete implementation
object Hello extends GreetingAlg[Option]{
  def greet = Some("Hello")
}

```

## Program
A program provides the evaluation of the dsl. In the example below the evaluation relies on a transformation provided by the ```Functor``` typeclass operation ```map``` to return a modified greeting. 

The ```implicit``` parameter ```F``` is a mechanism for accessing a provided ```Functor ``` for ```GreetingAlg```.

```scala
// define a program based on typeclass functions
object GreetingProgram{
  def program[F[_]](g:GreetingAlg[F])(implicit F:Functor[F]):F[String] = 
    F.map(g.greet)(_ + " :)")
}
// construct a program for Greeting algebra using the Option interpreter
val pr1:Option[String] = GreetingProgram.program(Hello)
```
## Running the dsl

The ```fold``` function yields a pure value from the ```Option ``` effect. The ```IO``` type provides the ```unsafeRunSync``` to run IO operations that yield a pure value — what it returns when it executes.

```scala
// fold effect to pure value
pr1.fold("Error")(identity) // Hello :)
```

## Combining dsls

A module can combine two interpreters. The ```Applicative``` typeclass can be used to map the common value.
```scala
object GreetingMessageModule{
  def program[F[_]](g:GreetingAlg[F], m:MessageAlg[F])(implicit F:Applicative[F]):F[String] = 
    F.map2(g.greet, m.message)((x, y) => x + " " + y)
}
```

## Recommended reading

* http://okmij.org/ftp/tagless-final/index.html

* https://leanpub.com/fpmortals

* https://oleksandrmanzyuk.wordpress.com/2014/06/18/from-object-algebras-to-finally-tagless-interpreters-2/

* https://wiki.haskell.org/wikiupload/e/e9/Typeclassopedia.pdf

* https://failex.blogspot.com/2016/12/tagless-final-effects-la-ermine-writers.html

* https://ro-che.info/articles/2016-02-03-finally-tagless-boilerplate

* https://typelevel.org/cats/nomenclature.html

* https://jproyo.github.io/posts/2019-02-07-practical-tagless-final-in-scala.html

* http://degoes.net/articles/tagless-horror